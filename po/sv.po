# Swedish translation for redshift
# Copyright (c) 2017
# This file is distributed under the same license as the redshift package.
# Jonathan Nyberg <EMAIL@ADDRESS>, 2017.
# Josef Andersson josef.andersson@fripost.org, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: redshift\n"
"Report-Msgid-Bugs-To: https://github.com/jonls/redshift/issues\n"
"POT-Creation-Date: 2020-09-04 12:22-0700\n"
"PO-Revision-Date: 2017-02-06 09:54+0000\n"
"Last-Translator: Josef Andersson <Unknown>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2018-05-21 01:04+0000\n"
"X-Generator: Launchpad (build 18658)\n"

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:1
#, fuzzy
msgid ""
"Adjusts the color temperature of your screen according to your surroundings. "
"This may help your eyes hurt less if you are working in front of the screen "
"at night."
msgstr ""
"Redshift justerar färgtemperaturen på skärmen utefter din omgivning. Det kan "
"göra att dina ögon mindre irriterade om du arbetar framför skärmen på natten."

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:2
msgid ""
"The color temperature is set according to the position of the sun. A "
"different color temperature is set during night and daytime. During twilight "
"and early morning, the color temperature transitions smoothly from night to "
"daytime temperature to allow your eyes to slowly adapt."
msgstr ""
"Färgtemperaturen ställs in beroende på positionen av solen. En annan "
"färgtemperatur sätts under natten och dagen. Under skymning och tidigt på "
"morgonen övergår färgtemperaturen smidigt från natt till dagtidstemperatur "
"för att ge dina ögon tid att långsamt anpassa sig."

#: ../data/appdata/gammastep-indicator.appdata.xml.in.h:3
#, fuzzy
msgid ""
"This program provides a status icon that allows the user to control color "
"temperature."
msgstr ""
"Detta program skapar en statusikon som gör det möjligt för användaren att "
"styra Redshift."

#: ../data/applications/gammastep.desktop.in.h:1
msgid "gammastep"
msgstr ""

#: ../data/applications/gammastep.desktop.in.h:2
msgid "Color temperature adjustment"
msgstr "Färgtemperaturjustering"

#: ../data/applications/gammastep.desktop.in.h:3
msgid "Color temperature adjustment tool"
msgstr "Justeringsverktyg för färgtemperaturer"

#: ../data/applications/gammastep-indicator.desktop.in.h:1
msgid "Gammastep Indicator"
msgstr ""

#: ../data/applications/gammastep-indicator.desktop.in.h:2
#, fuzzy
msgid "Indicator for color temperature adjustment"
msgstr "Färgtemperaturjustering"

#. TRANSLATORS: Name printed when period of day is unknown
#: ../src/redshift.c:92
msgid "None"
msgstr "Ingen"

#: ../src/redshift.c:93
msgid "Daytime"
msgstr "Dagtid"

#: ../src/redshift.c:94 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1022
msgid "Night"
msgstr "Natt"

#: ../src/redshift.c:95
msgid "Transition"
msgstr "Övergång"

#: ../src/redshift.c:178 ../src/gammastep_indicator/statusicon.py:313
#: ../src/gammastep_indicator/statusicon.py:326
msgid "Period"
msgstr "Period"

#: ../src/redshift.c:188 ../src/redshift.c:958 ../src/redshift.c:965
#: ../src/redshift.c:1018
#, fuzzy
msgid "Day"
msgstr "Dagtid"

#. TRANSLATORS: Abbreviation for `north'
#: ../src/redshift.c:198
msgid "N"
msgstr "N"

#. TRANSLATORS: Abbreviation for `south'
#: ../src/redshift.c:200
msgid "S"
msgstr "S"

#. TRANSLATORS: Abbreviation for `east'
#: ../src/redshift.c:202
msgid "E"
msgstr "Ö"

#. TRANSLATORS: Abbreviation for `west'
#: ../src/redshift.c:204
msgid "W"
msgstr "V"

#: ../src/redshift.c:206 ../src/gammastep_indicator/statusicon.py:319
msgid "Location"
msgstr "Plats"

#: ../src/redshift.c:280
#, fuzzy
msgid "Failed to initialize provider"
msgstr "Det gick inte att starta leverantör %s.\n"

#: ../src/redshift.c:294 ../src/redshift.c:334 ../src/redshift.c:378
#: ../src/redshift.c:404
#, fuzzy
msgid "Failed to set option"
msgstr "Det gick inte att ställa in flaggan %s.\n"

#: ../src/redshift.c:297 ../src/redshift.c:336
msgid "For more information, use option:"
msgstr ""

#: ../src/redshift.c:324 ../src/redshift.c:395
#, fuzzy
msgid "Failed to parse option"
msgstr "Det gick inte att tolka flaggan ”%s”.\n"

#: ../src/redshift.c:349
#, fuzzy
msgid "Failed to start provider"
msgstr "Det gick inte att starta leverantör %s.\n"

#: ../src/redshift.c:364
#, fuzzy
msgid "Failed to initialize method"
msgstr "Det gick inte att starta justeringsmetod %s.\n"

#: ../src/redshift.c:379 ../src/redshift.c:405
#, fuzzy
msgid "For more information, try:"
msgstr "Försök med ”-h” för mer information.\n"

#: ../src/redshift.c:417
#, fuzzy
msgid "Failed to start adjustment method"
msgstr "Det gick inte att starta justeringsmetod %s.\n"

#: ../src/redshift.c:445
#, fuzzy
msgid "Latitude must be in range"
msgstr "Latitud måste vara mellan %.1f och %.1f.\n"

#: ../src/redshift.c:452
#, fuzzy
msgid "Longitude must be in range:"
msgstr "Longitud måste vara mellan %.1f och %.1f.\n"

#: ../src/redshift.c:479 ../src/redshift.c:497 ../src/redshift.c:624
#: ../src/redshift.c:1096
#, fuzzy
msgid "Unable to read system time."
msgstr "Det går inte att läsa systemets tid.\n"

#: ../src/redshift.c:567
#, fuzzy
msgid "Waiting for initial location to become available..."
msgstr "Väntar på tillstånd att erhålla plats…\n"

#: ../src/redshift.c:572 ../src/redshift.c:763 ../src/redshift.c:777
#: ../src/redshift.c:1081
#, fuzzy
msgid "Unable to get location from provider."
msgstr "Det går inte att få plats från leverantör.\n"

#: ../src/redshift.c:577 ../src/redshift.c:800
#, fuzzy
msgid "Invalid location returned from provider."
msgstr "Det går inte att få plats från leverantör.\n"

#: ../src/redshift.c:584 ../src/redshift.c:715 ../src/redshift.c:1130
#: ../src/redshift.c:1155 ../src/gammastep_indicator/statusicon.py:307
#: ../src/gammastep_indicator/statusicon.py:325
msgid "Color temperature"
msgstr "Färgtemperatur"

#: ../src/redshift.c:585 ../src/redshift.c:718 ../src/redshift.c:1003
#: ../src/redshift.c:1131
#, fuzzy
msgid "Brightness"
msgstr "Ljusstyrka: %.2f\n"

#: ../src/redshift.c:614
#, fuzzy
msgid "Status"
msgstr "Status: %s\n"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:302
msgid "Disabled"
msgstr "Inaktiverad"

#: ../src/redshift.c:614 ../src/gammastep_indicator/statusicon.py:61
#: ../src/gammastep_indicator/statusicon.py:302
msgid "Enabled"
msgstr "Aktiverad"

#: ../src/redshift.c:727 ../src/redshift.c:1139 ../src/redshift.c:1163
#: ../src/redshift.c:1184
#, fuzzy
msgid "Temperature adjustment failed."
msgstr "Temperaturjustering misslyckades.\n"

#: ../src/redshift.c:783
msgid ""
"Location is temporarily unavailable; Using previous location until it "
"becomes available..."
msgstr ""

#: ../src/redshift.c:890
msgid "Partial time-configuration unsupported!"
msgstr ""

#: ../src/redshift.c:897
msgid "Invalid dawn/dusk time configuration!"
msgstr ""

#: ../src/redshift.c:926
#, fuzzy
msgid "Trying location provider"
msgstr "Provar platsleverantör ”%s”…\n"

#: ../src/redshift.c:931
#, fuzzy
msgid "Trying next provider..."
msgstr "Provar nästa leverantör…\n"

#: ../src/redshift.c:936
#, fuzzy
msgid "Using provider:"
msgstr "Använder leverantör ”%s”.\n"

#: ../src/redshift.c:944
#, fuzzy
msgid "No more location providers to try."
msgstr "Inga fler platsleverantörer att pröva.\n"

#: ../src/redshift.c:952
#, fuzzy
msgid ""
"High transition elevation cannot be lower than the low transition elevation."
msgstr ""
"Hög övergångsförhöjning kan inte vara lägre än den låga "
"övergångsförhöjningen.\n"

#: ../src/redshift.c:958
#, fuzzy
msgid "Solar elevations"
msgstr "Solförhöjning: %f\n"

#: ../src/redshift.c:965
#, fuzzy
msgid "Temperatures"
msgstr "Temperatur: %i\n"

#: ../src/redshift.c:975 ../src/redshift.c:986
#, fuzzy
msgid "Temperature must be in range"
msgstr "Latitud måste vara mellan %.1f och %.1f.\n"

#: ../src/redshift.c:997
#, fuzzy
msgid "Brightness must be in range"
msgstr "Latitud måste vara mellan %.1f och %.1f.\n"

#: ../src/redshift.c:1010
#, fuzzy
msgid "Gamma value must be in range"
msgstr "Latitud måste vara mellan %.1f och %.1f.\n"

#: ../src/redshift.c:1018 ../src/redshift.c:1022
msgid "Gamma"
msgstr ""

#: ../src/redshift.c:1049
#, fuzzy
msgid "Trying next method..."
msgstr "Provar nästa metod…\n"

#: ../src/redshift.c:1054
#, fuzzy
msgid "Using method"
msgstr "Använder metod ”%s”.\n"

#: ../src/redshift.c:1061
#, fuzzy
msgid "No more methods to try."
msgstr "Inga fler metoder att pröva.\n"

#: ../src/redshift.c:1075
#, fuzzy
msgid "Waiting for current location to become available..."
msgstr "Väntar på tillstånd att erhålla plats…\n"

#: ../src/redshift.c:1086
#, fuzzy
msgid "Invalid location from provider."
msgstr "Det går inte att få plats från leverantör.\n"

#: ../src/redshift.c:1112
#, fuzzy
msgid "Solar elevation"
msgstr "Solförhöjning: %f\n"

#: ../src/redshift.c:1147 ../src/redshift.c:1171 ../src/redshift.c:1192
#, fuzzy
msgid "Press ctrl-c to stop..."
msgstr "Tryck ctrl-c för att stoppa…\n"

#. TRANSLATORS: help output 1
#. LAT is latitude, LON is longitude,
#. DAY is temperature at daytime,
#. NIGHT is temperature at night
#. no-wrap
#: ../src/options.c:144
#, c-format
msgid "Usage: %s -l LAT:LON -t DAY:NIGHT [OPTIONS...]\n"
msgstr "Användning: %s -l LAT:LON -t DAG:NATT [FLAGGOR…]\n"

#. TRANSLATORS: help output 2
#. no-wrap
#: ../src/options.c:150
msgid "Set color temperature of display according to time of day.\n"
msgstr "Ställ in färgtemperatur på skärmen beroende av tid på dagen.\n"

#. TRANSLATORS: help output 3
#. no-wrap
#: ../src/options.c:156
#, fuzzy
msgid ""
"  -h\t\tDisplay this help message\n"
"  -v\t\tIncrease logging verbosity\n"
"  -q\t\tDecrease logging verbosity\n"
"  -V\t\tShow program version\n"
msgstr ""
"  -h\t\tVisa detta hjälp meddelande\n"
"  -v\t\tUtförligt utdata\n"
"  -V\t\tVisa programversion\n"

#. TRANSLATORS: help output 4
#. `list' must not be translated
#. no-wrap
#: ../src/options.c:165
msgid ""
"  -b DAY:NIGHT\tScreen brightness to apply (between 0.1 and 1.0)\n"
"  -c FILE\tLoad settings from specified configuration file\n"
"  -g R:G:B\tAdditional gamma correction to apply\n"
"  -l LAT:LON\tYour current location\n"
"  -l PROVIDER\tSelect provider for automatic location updates\n"
"  \t\t(Type `list' to see available providers)\n"
"  -m METHOD\tMethod to use to set color temperature\n"
"  \t\t(Type `list' to see available methods)\n"
"  -o\t\tOne shot mode (do not continuously adjust color temperature)\n"
"  -O TEMP\tOne shot manual mode (set color temperature)\n"
"  -p\t\tPrint mode (only print parameters and exit)\n"
"  -P\t\tReset existing gamma ramps before applying new color effect\n"
"  -x\t\tReset mode (remove adjustment from screen)\n"
"  -r\t\tDisable fading between color temperatures\n"
"  -t DAY:NIGHT\tColor temperature to set at daytime/night\n"
msgstr ""

#. TRANSLATORS: help output 5
#: ../src/options.c:187
#, c-format
msgid ""
"The neutral temperature is %uK. Using this value will not change the color\n"
"temperature of the display. Setting the color temperature to a value higher\n"
"than this results in more blue light, and setting a lower value will result "
"in\n"
"more red light.\n"
msgstr ""

#. TRANSLATORS: help output 6
#: ../src/options.c:196
#, c-format
msgid ""
"Default values:\n"
"\n"
"  Daytime temperature: %uK\n"
"  Night temperature: %uK\n"
msgstr ""
"Standardvärden:\n"
"\n"
"  Dagtidstemperatur: %uK\n"
"  Nattemperatur: %uK\n"

#. TRANSLATORS: help output 7
#: ../src/options.c:204
#, c-format
msgid "Please report bugs to <%s>\n"
msgstr "Vänligen rapportera fel till <%s>\n"

#: ../src/options.c:211
msgid "Available adjustment methods:\n"
msgstr "Tillgängliga justeringsmetoder:\n"

#: ../src/options.c:217
msgid "Specify colon-separated options with `-m METHOD:OPTIONS'.\n"
msgstr "Ange kolonseparerade flaggor med ”-m METOD:FLAGGOR”.\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:220
msgid "Try `-m METHOD:help' for help.\n"
msgstr "Försök med `-m METOD:help' för hjälp.\n"

#: ../src/options.c:227
msgid "Available location providers:\n"
msgstr "Tillgängliga platsleverantörer:\n"

#: ../src/options.c:233
msgid "Specify colon-separated options with`-l PROVIDER:OPTIONS'.\n"
msgstr "Ange kolonseparerade flaggor med ”-l LEVERANTÖR:FLAGGOR”.\n"

#. TRANSLATORS: `help' must not be translated.
#: ../src/options.c:236
msgid "Try `-l PROVIDER:help' for help.\n"
msgstr "Försök med ”-l LEVERANTÖR:help” för hjälp.\n"

#: ../src/options.c:337
msgid "Malformed gamma argument.\n"
msgstr "Felaktigt gamma-argument.\n"

#: ../src/options.c:338 ../src/options.c:450 ../src/options.c:473
msgid "Try `-h' for more information.\n"
msgstr "Försök med ”-h” för mer information.\n"

#: ../src/options.c:387 ../src/options.c:586
#, fuzzy
msgid "Unknown location provider"
msgstr "Okänd leverantörsplats ”%s”.\n"

#: ../src/options.c:419 ../src/options.c:576
#, fuzzy
msgid "Unknown adjustment method"
msgstr "Okänd justeringsmetod ”%s”.\n"

#: ../src/options.c:449
msgid "Malformed temperature argument.\n"
msgstr "Felaktigt temperaturargument.\n"

#: ../src/options.c:545 ../src/options.c:557 ../src/options.c:566
msgid "Malformed gamma setting.\n"
msgstr "Felaktigt gamma-inställning.\n"

#: ../src/options.c:596
#, fuzzy
msgid "Malformed dawn-time setting"
msgstr "Felaktigt gamma-inställning.\n"

#: ../src/options.c:606
#, fuzzy
msgid "Malformed dusk-time setting"
msgstr "Felaktigt gamma-inställning.\n"

#: ../src/options.c:612
#, fuzzy
msgid "Unknown configuration setting"
msgstr "Okänd konfigurationsinställning ”%s”.\n"

#: ../src/config-ini.c:143
msgid "Malformed section header in config file.\n"
msgstr "Felaktig avsnittsrubrik i konfigurationsfil.\n"

#: ../src/config-ini.c:179
msgid "Malformed assignment in config file.\n"
msgstr "Felaktigt tilldelning i konfigurationsfil.\n"

#: ../src/config-ini.c:190
msgid "Assignment outside section in config file.\n"
msgstr "Tilldelning utanför avsnitt i konfigurationsfil.\n"

#: ../src/gamma-drm.c:86
#, c-format
msgid "Failed to open DRM device: %s\n"
msgstr ""

#: ../src/gamma-drm.c:94
#, c-format
msgid "Failed to get DRM mode resources\n"
msgstr "Det gick inte att få DRM-läges resurser\n"

#: ../src/gamma-drm.c:104 ../src/gamma-randr.c:373
#, c-format
msgid "CRTC %d does not exist. "
msgstr "CRTC %d existerar inte. "

#: ../src/gamma-drm.c:107 ../src/gamma-randr.c:376
#, c-format
msgid "Valid CRTCs are [0-%d].\n"
msgstr "Giltiga CRTC är [0-%d].\n"

#: ../src/gamma-drm.c:110 ../src/gamma-randr.c:379
#, c-format
msgid "Only CRTC 0 exists.\n"
msgstr "Bara CRTC 0 finns.\n"

#: ../src/gamma-drm.c:148
#, c-format
msgid "CRTC %i lost, skipping\n"
msgstr "CRTC %i förlorat, hoppar över\n"

#: ../src/gamma-drm.c:154
#, c-format
msgid ""
"Could not get gamma ramp size for CRTC %i\n"
"on graphics card %i, ignoring device.\n"
msgstr ""
"Det gick inte att hämta gammaförfiningars storlek för CRTC %i\n"
"på grafikkort %i, ignorerar enhet.\n"

#: ../src/gamma-drm.c:167
#, c-format
msgid ""
"DRM could not read gamma ramps on CRTC %i on\n"
"graphics card %i, ignoring device.\n"
msgstr ""
"DRM kunde inte läsa gammaförfiningar på CRTC %i på\n"
"grafikkort %i, ignorerar enhet.\n"

#: ../src/gamma-drm.c:231
msgid "Adjust gamma ramps with Direct Rendering Manager.\n"
msgstr "Justera gammaförfiningar med Direct Rendering Manager.\n"

#. TRANSLATORS: DRM help output
#. left column must not be translated
#: ../src/gamma-drm.c:236
msgid ""
"  card=N\tGraphics card to apply adjustments to\n"
"  crtc=N\tCRTC to apply adjustments to\n"
msgstr ""
"  card=N\tGrafikkort att tillämpa justeringar på\n"
"  crtc=N\tCRTC att tillämpa justeringar på\n"

#: ../src/gamma-drm.c:249
#, c-format
msgid "CRTC must be a non-negative integer\n"
msgstr "CRTC måste vara ett icke-negativt heltal\n"

#: ../src/gamma-drm.c:253 ../src/gamma-randr.c:358 ../src/gamma-vidmode.c:150
#: ../src/gamma-dummy.c:56
#, c-format
msgid "Unknown method parameter: `%s'.\n"
msgstr "Okänd metod-parameter: ”%s”.\n"

#: ../src/gamma-wl.c:70
msgid "Not authorized to bind the wlroots gamma control manager interface."
msgstr ""

#: ../src/gamma-wl.c:90
#, fuzzy
msgid "Failed to allocate memory"
msgstr "Det gick inte att starta justeringsmetod %s.\n"

#: ../src/gamma-wl.c:124
msgid "The zwlr_gamma_control_manager_v1 was removed"
msgstr ""

#: ../src/gamma-wl.c:177
msgid "Could not connect to wayland display, exiting."
msgstr ""

#: ../src/gamma-wl.c:218
msgid "Ignoring Wayland connection error while waiting to disconnect"
msgstr ""

#: ../src/gamma-wl.c:247
#, fuzzy
msgid "Adjust gamma ramps with a Wayland compositor.\n"
msgstr "Justera gammaförfiningar med Windows GDI.\n"

#: ../src/gamma-wl.c:282
msgid "Wayland connection experienced a fatal error"
msgstr ""

#: ../src/gamma-wl.c:347
msgid "Zero outputs support gamma adjustment."
msgstr ""

#: ../src/gamma-wl.c:352
msgid "output(s) do not support gamma adjustment"
msgstr ""

#: ../src/gamma-randr.c:83 ../src/gamma-randr.c:142 ../src/gamma-randr.c:181
#: ../src/gamma-randr.c:207 ../src/gamma-randr.c:264 ../src/gamma-randr.c:424
#, c-format
msgid "`%s' returned error %d\n"
msgstr "”%s” returnerade ett fel %d\n"

#: ../src/gamma-randr.c:92
#, c-format
msgid "Unsupported RANDR version (%u.%u)\n"
msgstr "RANDR-version stöds ej (%u.%u)\n"

#: ../src/gamma-randr.c:127
#, c-format
msgid "Screen %i could not be found.\n"
msgstr "Skärm %i kunde inte hittas.\n"

#: ../src/gamma-randr.c:193 ../src/gamma-vidmode.c:85
#, c-format
msgid "Gamma ramp size too small: %i\n"
msgstr "Gammaförfinings storlek för liten: %i\n"

#: ../src/gamma-randr.c:266
#, c-format
msgid "Unable to restore CRTC %i\n"
msgstr "Det går inte att återställa CRTC %i\n"

#: ../src/gamma-randr.c:290
msgid "Adjust gamma ramps with the X RANDR extension.\n"
msgstr "Justera gammaförfiningar med X RANDR utökningen.\n"

#. TRANSLATORS: RANDR help output
#. left column must not be translated
#: ../src/gamma-randr.c:295
msgid ""
"  screen=N\t\tX screen to apply adjustments to\n"
"  crtc=N\tList of comma separated CRTCs to apply adjustments to\n"
msgstr ""

#: ../src/gamma-randr.c:317
#, c-format
msgid "Unable to read screen number: `%s'.\n"
msgstr ""

#: ../src/gamma-randr.c:353 ../src/gamma-vidmode.c:145
#, c-format
msgid ""
"Parameter `%s` is now always on;  Use the `%s` command-line option to "
"disable.\n"
msgstr ""

#: ../src/gamma-vidmode.c:50 ../src/gamma-vidmode.c:70
#: ../src/gamma-vidmode.c:79 ../src/gamma-vidmode.c:106
#: ../src/gamma-vidmode.c:169 ../src/gamma-vidmode.c:214
#, c-format
msgid "X request failed: %s\n"
msgstr "X-anrop misslyckades: %s\n"

#: ../src/gamma-vidmode.c:129
msgid "Adjust gamma ramps with the X VidMode extension.\n"
msgstr "Justera gammaförfiningar med X VidMode utökning.\n"

#. TRANSLATORS: VidMode help output
#. left column must not be translated
#: ../src/gamma-vidmode.c:134
msgid "  screen=N\t\tX screen to apply adjustments to\n"
msgstr ""

#: ../src/gamma-dummy.c:32
msgid ""
"WARNING: Using dummy gamma method! Display will not be affected by this "
"gamma method.\n"
msgstr ""
"VARNING: Använder falsk gamma-metod! Visning kommer inte att påverkas av "
"denna gamma-metod.\n"

#: ../src/gamma-dummy.c:49
msgid ""
"Does not affect the display but prints the color temperature to the "
"terminal.\n"
msgstr ""
"Påverkar inte visningen men skriver färgtemperaturen till terminalen.\n"

#: ../src/gamma-dummy.c:64
#, c-format
msgid "Temperature: %i\n"
msgstr "Temperatur: %i\n"

#: ../src/location-geoclue2.c:49
#, c-format
msgid ""
"Access to the current location was denied!\n"
"Ensure location services are enabled and access by this program is "
"permitted.\n"
msgstr ""

#: ../src/location-geoclue2.c:95
#, c-format
msgid "Unable to obtain location: %s.\n"
msgstr "Det går inte att erhålla plats: %s.\n"

#: ../src/location-geoclue2.c:138
#, c-format
msgid "Unable to obtain GeoClue Manager: %s.\n"
msgstr "Det går inte att erhålla GeoClue-hanterare: %s.\n"

#: ../src/location-geoclue2.c:154
#, c-format
msgid "Unable to obtain GeoClue client path: %s.\n"
msgstr "Det går inte att erhålla GeoClue-klients sökväg: %s.\n"

#: ../src/location-geoclue2.c:176
#, c-format
msgid "Unable to obtain GeoClue Client: %s.\n"
msgstr "Det går inte att erhålla GeoClue-klient: %s.\n"

#: ../src/location-geoclue2.c:217
#, c-format
msgid "Unable to set distance threshold: %s.\n"
msgstr "Det går inte att ställa in avståndsgränsvärdet: %s.\n"

#: ../src/location-geoclue2.c:241
#, c-format
msgid "Unable to start GeoClue client: %s.\n"
msgstr "Det går inte att starta GeoClue-klient: %s.\n"

#: ../src/location-geoclue2.c:380
msgid "GeoClue2 provider is not installed!"
msgstr ""

#: ../src/location-geoclue2.c:387
#, fuzzy
msgid "Failed to start GeoClue2 provider!"
msgstr "Det gick inte att starta leverantör %s.\n"

#: ../src/location-geoclue2.c:422
msgid "Use the location as discovered by a GeoClue2 provider.\n"
msgstr "Använd platsen som upptäcktes av en GeoClue2-leverantör.\n"

#: ../src/location-geoclue2.c:431 ../src/location-manual.c:96
#, fuzzy
msgid "Unknown method parameter"
msgstr "Okänd metod-parameter: ”%s”.\n"

#: ../src/location-manual.c:49
#, fuzzy
msgid "Latitude and longitude must be set."
msgstr "Latitud och longitud måste anges.\n"

#: ../src/location-manual.c:65
msgid "Specify location manually.\n"
msgstr "Ange plats manuellt.\n"

#. TRANSLATORS: Manual location help output
#. left column must not be translated
#: ../src/location-manual.c:70
msgid ""
"  lat=N\t\tLatitude\n"
"  lon=N\t\tLongitude\n"
msgstr ""
"  lat=N\t\tLatitud\n"
"  lon=N\t\tLongitud\n"

#: ../src/location-manual.c:73
msgid ""
"Both values are expected to be floating point numbers,\n"
"negative values representing west / south, respectively.\n"
msgstr ""
"Båda värdena beräknas vara flyttal,\n"
"negativa värden representerar väst/söder.\n"

#: ../src/location-manual.c:87
msgid "Malformed argument.\n"
msgstr "Felaktigt argument.\n"

#: ../src/gammastep_indicator/statusicon.py:66
msgid "Suspend for"
msgstr "Uppskjuta för"

#: ../src/gammastep_indicator/statusicon.py:68
msgid "30 minutes"
msgstr "30 minuter"

#: ../src/gammastep_indicator/statusicon.py:69
msgid "1 hour"
msgstr "1 timme"

#: ../src/gammastep_indicator/statusicon.py:70
msgid "2 hours"
msgstr "2 timmar"

#: ../src/gammastep_indicator/statusicon.py:71
#, fuzzy
msgid "4 hours"
msgstr "2 timmar"

#: ../src/gammastep_indicator/statusicon.py:72
#, fuzzy
msgid "8 hours"
msgstr "2 timmar"

#: ../src/gammastep_indicator/statusicon.py:81
msgid "Autostart"
msgstr "Starta automatiskt"

#: ../src/gammastep_indicator/statusicon.py:93
#: ../src/gammastep_indicator/statusicon.py:103
msgid "Info"
msgstr "Info"

#: ../src/gammastep_indicator/statusicon.py:98
msgid "Quit"
msgstr "Avsluta"

#: ../src/gammastep_indicator/statusicon.py:136
msgid "Close"
msgstr "Stäng"

#: ../src/gammastep_indicator/statusicon.py:301
msgid "<b>Status:</b> {}"
msgstr "<b>Status:</b> {}"

#: ../src/gammastep_indicator/statusicon.py:349
msgid "For help output, please run:"
msgstr ""

#, fuzzy, c-format
#~ msgid "Color temperature: %uK"
#~ msgstr "Färgtemperatur: %uK\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f"
#~ msgstr "Ljusstyrka: %.2f\n"

#, fuzzy, c-format
#~ msgid "Solar elevations: day above %.1f, night below %.1f"
#~ msgstr "Solhöjd: dag över %.1f, natt under %.1f\n"

#, fuzzy, c-format
#~ msgid "Temperatures: %dK at day, %dK at night"
#~ msgstr "Temperaturer: %dK vid dag, %dK vid natt\n"

#, fuzzy, c-format
#~ msgid "Temperature must be between %uK and %uK."
#~ msgstr "Temperatur måste vara mellan %uK och %uK.\n"

#, fuzzy, c-format
#~ msgid "Brightness values must be between %.1f and %.1f."
#~ msgstr "Värden för ljusstyrka måste vara mellan %.1f och %.1f.\n"

#, fuzzy, c-format
#~ msgid "Brightness: %.2f:%.2f"
#~ msgstr "Ljusstyrka: %.2f:%.2f\n"

#, fuzzy, c-format
#~ msgid "Gamma value must be between %.1f and %.1f."
#~ msgstr "Gammavärde måste vara mellan %.1f och %.1f.\n"

#, fuzzy, c-format
#~ msgid "Gamma (%s): %.3f, %.3f, %.3f"
#~ msgstr "Gamma (%s): %.3f, %.3f, %.3f\n"

#, fuzzy, c-format
#~ msgid "Period: %s"
#~ msgstr "Period: %s\n"

#, fuzzy, c-format
#~ msgid "Period: %s (%.2f%% day)"
#~ msgstr "Period: %s (%.2f%% dag)\n"

#, fuzzy, c-format
#~ msgid "Initialization of %s failed."
#~ msgstr "Initiering av %s misslyckades.\n"

#, fuzzy, c-format
#~ msgid "Try `-l %s:help' for more information."
#~ msgstr "Försök med ”-l %s:help” för mera information.\n"

#, fuzzy, c-format
#~ msgid "Try `-m %s:help' for more information."
#~ msgstr "Försök med ”-m %s:help” för mer information.\n"

#, fuzzy, c-format
#~ msgid "Try -m %s:help' for more information."
#~ msgstr "Försök med ”-m %s:help” för mer information.\n"

#, fuzzy
#~ msgid "redshift"
#~ msgstr "Redshift"

#, c-format
#~ msgid "Location: %.2f %s, %.2f %s\n"
#~ msgstr "Plats: %.2f %s, %.2f %s\n"

#~ msgid ""
#~ "The Redshift information window overlaid with an example of the redness "
#~ "effect"
#~ msgstr ""
#~ "Informationsfönstret för Redshift övertäckt med ett exempel på "
#~ "rödhetseffekten"

#~ msgid "Unable to save current gamma ramp.\n"
#~ msgstr "Det går inte att spara nuvarande gammaförfining.\n"

#~ msgid "Unable to open device context.\n"
#~ msgstr "Det går inte att öppna enhetskontext.\n"

#~ msgid "Display device does not support gamma ramps.\n"
#~ msgstr "Visningsenhet stöder inte gammaförfiningar.\n"

#~ msgid "Unable to restore gamma ramps.\n"
#~ msgstr "Det går inte att återställa gammaförfiningar.\n"

#~ msgid "Unable to set gamma ramps.\n"
#~ msgstr "Det går inte att ställa in gammaförfiningar.\n"

#~ msgid "Not authorized to obtain location from CoreLocation.\n"
#~ msgstr "Inte behörighet att få plats från CoreLocation.\n"

#, c-format
#~ msgid "Error obtaining location from CoreLocation: %s\n"
#~ msgstr "Fel erhållande av plats från CoreLocation: %s\n"

#~ msgid "Request for location was not authorized!\n"
#~ msgstr "Begäran om platsen var inte godkänt!\n"

#~ msgid "Use the location as discovered by the Corelocation provider.\n"
#~ msgstr "Använd platsen som upptäckts av Corelocation-leverantören.\n"
