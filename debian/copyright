Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gammastep
Upstream-Contact: Jon Lund Steffensen <jonlst@gmail.com>
Source: https://gitlab.com/chinstrap/gammastep

Files: *
Copyright:
 2009-2018 Jon Lund Steffensen <jonlst@gmail.com>
License: GPL-3+

Files: data/apparmor/com.gitlab.chinstrap.gammastep.in
Copyright:
 2015-2020 Cameron Nemo <cnemo@tutanota.com>
License: 0BSD

Files: po/*.po
Copyright:
 2010-2018 Rosetta Contributors
 2010-2018 Canonical Ltd
License: GPL-3+

Files: po/sv.po
Copyright:
 2017 Jonathan Nyberg
 2017 Josef Andersson <josef.andersson@fripost.org>
License: GPL-3+

Files: src/colorramp.c
Copyright:
 2013 Ingo Thies <ithies@astro.uni-bonn.de>
License: GPL-3+

Files: src/gamma-drm.*
Copyright:
 2014 Mattias Andrée <maandree@member.fsf.org>
 2017 Jon Lund Steffensen <jonlst@gmail.com>
License: GPL-3+

Files: src/gamma-wl.*
Copyright:
 2015 Giulio Camuffo <giuliocamuffo@gmail.com>
License: GPL-3+

Files: src/gammastep_indicator/utils.py
Copyright:
 2010 Francesco Marella <francesco.marella@gmail.com>
 2011 Jon Lund Steffensen <jonlst@gmail.com>
License: GPL-3+

Files: src/signals.*
Copyright:
 2009-2015 Jon Lund Steffensen <jonlst@gmail.com>
 2015 Mattias Andrée <maandree@member.fsf.org>
License: GPL-3+

Files: src/os-compatibility.c
Copyright: 2012 Collabora, Ltd.
License: MIT

Files: src/vlog.*
Copyright: unknown
License: 0BSD

Files: src/gamma-control.xml
Copyright:
 2015 Giulio camuffo
 2018 Simon Ser
License: MIT-CMU

Files: debian/*
Copyright: 2020 Felix Lechner <felix.lechner@lease-up.com>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 See the file /usr/share/common-licenses/GPL-3 for details, or visit
 <http://www.fsf.org/licenses/licenses.html>.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice (including the
 next paragraph) shall be included in all copies or substantial
 portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: MIT-CMU
 Permission to use, copy, modify, distribute, and sell this
 software and its documentation for any purpose is hereby granted
 without fee, provided that the above copyright notice appear in
 all copies and that both that copyright notice and this permission
 notice appear in supporting documentation, and that the name of
 the copyright holders not be used in advertising or publicity
 pertaining to distribution of the software without specific,
 written prior permission.  The copyright holders make no
 representations about the suitability of this software for any
 purpose.  It is provided "as is" without express or implied
 warranty.
 .
 THE COPYRIGHT HOLDERS DISCLAIM ALL WARRANTIES WITH REGARD TO THIS
 SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS, IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
 THIS SOFTWARE.

License: 0BSD
 Permission to use, copy, modify, and/or distribute this software
 for any purpose with or without fee is hereby granted.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA
 OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOFTWARE.
